#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>



class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void addShape(Shape& s);
	bool removeShape(string name);
	void draw(string name);
	void clearDraw();
	void editShape(string name);


private: 
	Canvas _canvas;
	vector<Shape> _shapes;
};


#include "Triangle.h"


//////////////////////////////////////////////////////////////////////////////
// Canvas and Cimg cannot be used on Linux, 								//
// please make sure to leave it commented if you want test to run on GitLab //
// You can remove comments when you run your exercise locally on Windows .  //
//////////////////////////////////////////////////////////////////////////////

Triangle::Triangle(Point a, Point b, Point c, std::string type, std::string name) : Polygon(type, name) {
	// Checking a, b, and c form a triangle - if the area is equal to 0, they do not.
	double area = a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY()) + c.getX() * (a.getY() - b.getY());
	if (area == 0) {
		cout << "The given points do not form a triangle" << endl;
		throw this;
	}
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle() {}

double Triangle::getArea() const {
	// Variables for readability:
	Point a = _points[0];
	Point b = _points[1];
	Point c = _points[2];
	double area = a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY()) + c.getX() * (a.getY() - b.getY());
	return abs(area); // Result could be negative 
}

double Triangle::getPerimeter() const{
	return _points[0].distance(_points[1]) + _points[0].distance(_points[2]) + _points[1].distance(_points[2]);
}


void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

#include "Arrow.h"

//////////////////////////////////////////////////////////////////////////////
// Canvas and Cimg cannot be used on Linux, 								//
// please make sure to leave it commented if you want test to run on GitLab //
// You can remove comments when you run your exercise locally on Windows .  //
//////////////////////////////////////////////////////////////////////////////

Arrow::Arrow(Point a, Point b, std::string type, std::string name): Shape(type, name) {
	if (a == b) {
		cout << "The same point has been provided twice, thus a line cannot be drawn" << endl;
		throw this;
	}
	// else not needed as the program would crash
	source = a;
	destination = b;
}

Arrow::~Arrow(){}

Point Arrow::getSource() const {
	return source;
}

Point Arrow::getDestination() const {
	return destination;
}

double Arrow::getArea() const {
	return 0;
}

double Arrow::getPerimeter() const {
	return source.distance(destination);
}

void Arrow::move(const Point& p){
	source += p;
	destination += p;
}

void Arrow::draw(const Canvas& canvas) const
{
	canvas.draw_arrow(source, destination);
}
void Arrow::clearDraw(const Canvas& canvas) const
{
	canvas.clear_arrow(source, destination);
}



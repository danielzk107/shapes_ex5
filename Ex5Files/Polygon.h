#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:

	// Constructor
	Polygon(std::string type, std::string name);

	// Destructor
	~Polygon();

	// Getters
	vector<Point> getPoints() const;
	virtual double getPerimeter() const;
	virtual double getArea() const;
	virtual void draw(const Canvas& canvas) = 0;
	virtual void clearDraw(const Canvas& canvas) = 0;
	virtual void move(const Point& p) = 0;

protected:
	vector<Point> _points;
};
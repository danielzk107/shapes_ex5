#include "Rectangle.h"

//////////////////////////////////////////////////////////////////////////////
// Canvas and Cimg cannot be used on Linux, 								//
// please make sure to leave it commented if you want test to run on GitLab //
// You can remove comments when you run your exercise locally on Windows .  //
//////////////////////////////////////////////////////////////////////////////

myShapes::Rectangle::Rectangle(Point a, double length, double width, std::string type, std::string name) : Polygon(type, name){
	// Variables for readability
	double x = a.getX();
	double y = a.getY();
	_points.push_back(a);
	_points.push_back(Point(x +length, y + length));
	_points.push_back(Point(x +length, y + width));
	_points.push_back(Point(x + width, y + width));
	this->length = length;
	this->width = width;
}

myShapes::Rectangle::~Rectangle() {}

double myShapes::Rectangle::getArea() const {
	return length * width;
}

double myShapes::Rectangle::getPerimeter() const {
	return length * 2 + width * 2;
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}



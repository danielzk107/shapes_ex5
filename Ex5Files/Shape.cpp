#include "Shape.h"


Shape::Shape(string name, string type) {
	_name = name;
	_type = type;
}

Shape::~Shape(){}

string Shape::getType() const {
	return _type;
}

string Shape::getName() const {
	return _name;
}

void Shape::printDetails() const {
	cout << "Name: " << _name << ", Type: " << _type << endl;
}
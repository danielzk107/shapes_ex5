#include "Menu.h"

Menu::Menu() 
{
}

Menu::~Menu()
{
}

void Menu::addShape(Shape& s) {
	_shapes.push_back(s);
}


bool Menu::removeShape(string name) {
	for (int i = 0; i < _shapes.size(); i++)
	{
		if (_shapes[i].getName() == name) {
			_shapes.erase(_shapes.begin() + i);
			return true;
		}
	}
	return false;
}

void Menu::draw(string name) {
	for (int i = 0; i < _shapes.size(); i++)
	{
		if (_shapes[i].getName() == name) {
			_shapes[i].draw(_canvas);
			return;
		}
	}
}

void Menu::clearDraw() {
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i].clearDraw(_canvas);
	}
}

void editShape(string name);


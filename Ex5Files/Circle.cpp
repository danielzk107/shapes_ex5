#include "Circle.h"


//////////////////////////////////////////////////////////////////////////////
// Canvas and Cimg cannot be used on Linux, 								//
// please make sure to leave it commented if you want test to run on GitLab //
// You can remove comments when you run your exercise locally on Windows .  //
//////////////////////////////////////////////////////////////////////////////

Circle::Circle(Point center, double radius, std::string type, std::string name) : Shape(type, name) {
	centre = center;
	this->radius = radius;
}

Circle::~Circle() {}

Point Circle::getCenter() const {
	return centre;
}

double Circle::getRadius() const {
	return radius;
}

double Circle::getPerimeter() const {
	return radius * 2 * PI;
}

double Circle::getArea() const {
	return pow(radius, 2) * PI;
}

void Circle::move(const Point& p) {
	centre += p;
}

void Circle::draw(const Canvas& canvas) 
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}



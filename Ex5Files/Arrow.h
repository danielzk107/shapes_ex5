#pragma once
#include "Shape.h"
#include "Canvas.h"

class Arrow : public Shape
{
public:

	// Constructor
	Arrow(Point a, Point b, std::string type, std::string name);

	// Destructor
	~Arrow();

	// Getters
	Point getSource() const;
	Point getDestination() const;

	//////////////////////////////////////////////////////////////////////////////
	// Canvas and Cimg cannot be used on Linux, 								//
	// please make sure to leave it commented if you want test to run on GitLab //
	// You can remove comments when you run your exercise locally on Windows .  //
	//////////////////////////////////////////////////////////////////////////////
	double getArea() const override;
	double getPerimeter() const override;
	void move(const Point& p) override;
	void draw(const Canvas& canvas) const override;
	void clearDraw(const Canvas& canvas) const override;


private:
	Point source, destination;
};
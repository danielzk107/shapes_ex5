#pragma once

#include "Shape.h"
#include "Point.h"
#include "Canvas.h"

#define PI 3.14

class Circle : public Shape
{
public:

	// Constructor
	Circle(Point center, double radius, std::string type, std::string name);

	// Destructor
	~Circle();

	// Getters
	Point getCenter() const;
	double getRadius() const;
	double getPerimeter() const override;
	double getArea() const override;

	//////////////////////////////////////////////////////////////////////////////
	// Canvas and Cimg cannot be used on Linux, 								//
	// please make sure to leave it commented if you want test to run on GitLab //
	// You can remove comments when you run your exercise locally on Windows .  //
	//////////////////////////////////////////////////////////////////////////////

	void move(const Point& p) override;
	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

private:
	Point centre;
	double radius;
};
#include "Point.h"
#include <math.h>


Point::Point() {
	x = 0;
	y = 0;
}

Point::Point(double x, double y) {
	this->x = x;
	this->y = y;
}

Point::~Point(){}

Point Point::operator+(const Point& other) const {
	Point output(this->x + other.x, this->y + other.y);
	return output;
}

Point& Point::operator+=(const Point& other) {
	this->x += other.x;
	this->y += other.y;
	return *this;
}

bool Point::operator==(const Point& other) const {
	return this->x == other.x && this->y == other.y;
}

double Point::getX() const {
	return x;
}

double Point::getY() const {
	return y;
}

double Point::distance(const Point& other) const{
	int new_x = pow((this->x - other.x), 2);
	int new_y = pow((this->y - other.y), 2);
	return sqrt(new_x + new_y);
}
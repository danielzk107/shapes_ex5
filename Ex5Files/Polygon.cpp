#include "Polygon.h"

Polygon::Polygon(std::string type, std::string name) : Shape(type, name) {}

Polygon:: ~Polygon() {}

vector<Point> Polygon::getPoints() const {
	return _points;
}

double Polygon::getArea() const {
	return 0;
}

double Polygon::getPerimeter() const {
	return 0;
}